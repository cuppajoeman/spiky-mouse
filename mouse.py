import serial
import pyautogui
import numpy as np

# LINUX
arduino = serial.Serial('/dev/ttyACM0', 115200, timeout=.1)
# WINDOWS
#arduino = serial.Serial('COM3', 115200, timeout=.1)

pyautogui.PAUSE = 0
# We don't care if we land in a corne
pyautogui.FAILSAFE = False

keysDown = {}

def empty_function():
	pass

def simple_polynomial(x, n):
	return x ** n

def exponent(base, exponent):
	return base ** exponent - 1



class Mouse:
    def __init__(self, base_speed_coefficient, movement_function, buttons):
        self.base_speed_coefficient = base_speed_coefficient
        self.speed_coefficient = base_speed_coefficient
        self.alternative_coeffs = [self.speed_coefficient, self.speed_coefficient * 2, self.speed_coefficient/2]
        self.movement_function = movement_function
        self.buttons = buttons


        self.auto_scrolling = False
        self.auto_scroll_speed = SCROLL_SPEED
        self.auto_scroll_direction = -1

        self.mode = "SCROLL"

        self.set_initial_button_state()

        self.ticks_alive = 0


    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def run_mouse_movement(self, x, y):
        
        if not (abs(x/512)<= 0.1 and abs(y/512)<= 0.1):
            movement_vector = np.array([x,y])
            norm = np.linalg.norm(movement_vector)
            print(norm)
            normalized_movement_vector = movement_vector / norm
            normalized_x = normalized_movement_vector[0]
            normalized_y = normalized_movement_vector[1]

            print("joystick percentage: ", normalized_x, normalized_y)

            scaled_movement_vector = normalized_movement_vector * self.speed_coefficient 

            mouse_movement_x = scaled_movement_vector[0]
            mouse_movement_y = scaled_movement_vector[1]

            print("after mapping: ", mouse_movement_x, mouse_movement_y)


            pyautogui.moveRel(mouse_movement_x, mouse_movement_y)


    def perform_automatic_operations(self):
        if self.auto_scrolling:
            # slow down the scrolling
            if self.ticks_alive % 2 == 0:
                pyautogui.scroll(self.auto_scroll_direction * self.auto_scroll_speed)



    def cycle_mouse_speed(self):
        i = self.alternative_coeffs.index(self.speed_coefficient)
        self.speed_coefficient = self.alternative_coeffs[(i+1) % len(self.alternative_coeffs)]

    def set_mouse_speed(self, option):
        if option == "INCREASE":
            self.speed_coefficient = self.speed_coefficient * 4
        if option == "DECREASE":
            self.speed_coefficient = self.speed_coefficient / 4
        if option == "RESET":
            self.speed_coefficient = self.base_speed_coefficient 

    def set_auto_scroll_speed(self, option):
        print(option)
        if option == "INCREASE":
            self.auto_scroll_speed = int(self.auto_scroll_speed * 1.5)
        if option == "DECREASE":
            self.auto_scroll_speed = int(self.auto_scroll_speed / 1.5)
        if option == "FLIP":
            self.auto_scroll_direction = self.auto_scroll_direction * -1
        if option == "RESET":
            self.auto_scroll_speed = -SCROLL_SPEED
            self.auto_scroll_direction = 1

        print(self.auto_scroll_speed)

    def set_initial_button_state(self):
        print("resetting buttons")
        labels = ['A', 'B', 'C', 'D', 'E', 'F']

        self.buttons['A'] = Button('A', False, pyautogui.mouseDown, pyautogui.mouseUp)
        self.buttons['B'] = Button('B', False, lambda: pyautogui.click(button='right'), empty_function)
        self.buttons['C'] = Button('C', True, lambda: pyautogui.scroll(-SCROLL_SPEED), empty_function)
        self.buttons['D'] = Button('D', True, lambda: pyautogui.scroll(+SCROLL_SPEED), empty_function)
        self.buttons['E'] = Button('E', False, lambda: self.set_mouse_speed("DECREASE"), lambda: self.set_mouse_speed("RESET"))
        self.buttons['F'] = Button('F', False, lambda: self.switch_to_auto_scroll_mode() if self.buttons['F'].times_pressed % 2 == 1 else  self.reset_mouse_state(), empty_function)
        self.buttons['JS_DOWN'] = Button('JS_DOWN', False, lambda: self.switch_to_scroll_mode() if self.buttons['JS_DOWN'].times_pressed % 2 == 1 else  self.switch_to_speed_control_mode(), empty_function)
        self.auto_scrolling = False

    def reset_mouse_state(self):
        self.reset_button_state()
        self.auto_scrolling = False

    def reset_button_state(self):
        labels = ['A', 'B', 'C', 'D', 'E', 'F']
        for l in labels:
            self.buttons[l].reset_button_settings()
            


    def switch_to_speed_control_mode(self):
        self.buttons["D"].downAction =  lambda: self.set_mouse_speed("INCREASE")
        self.buttons["D"].upAction = lambda: self.set_mouse_speed("RESET")
        self.buttons["D"].repeating = False

        self.buttons["C"].downAction =  lambda: self.set_mouse_speed("DECREASE")
        self.buttons["C"].upAction = lambda: self.set_mouse_speed("RESET")
        self.buttons["C"].repeating = False

    def switch_to_scroll_mode(self):
        self.buttons["D"].downAction =  lambda: pyautogui.scroll(+SCROLL_SPEED)
        self.buttons["D"].upAction = empty_function
        self.buttons["D"].repeating = True

        self.buttons["C"].downAction =  lambda: pyautogui.scroll(-SCROLL_SPEED)
        self.buttons["C"].upAction = empty_function
        self.buttons["C"].repeating = True

    def switch_to_auto_scroll_mode(self):
        print("switching to auto scroll")
        # Increase speed
        self.buttons["A"].downAction =  lambda: self.set_auto_scroll_speed("INCREASE")
        self.buttons["A"].upAction = empty_function
        self.buttons["A"].repeating = True

        # decrease speed
        self.buttons["C"].downAction =  lambda: self.set_auto_scroll_speed("DECREASE")
        self.buttons["C"].upAction = empty_function
        self.buttons["C"].repeating = True

        # switch direction
        self.buttons["B"].downAction =  lambda: self.set_auto_scroll_speed("FLIP")
        self.buttons["B"].upAction = empty_function
        self.buttons["B"].repeating = False

        self.buttons["D"].downAction =  lambda: self.set_auto_scroll_speed("RESET")
        self.buttons["D"].upAction = empty_function
        self.buttons["D"].repeating = False

        self.auto_scrolling = True




class Button:
    def __init__(self, label, repeating, downAction, upAction):
        self.label = label
        # This should only apply to mouse going down
        self.repeating = repeating
        self.downAction = downAction
        self.upAction = upAction
        self.pressed = False
        self.times_pressed = 0
        self.orig_settings = (label, repeating, downAction, upAction)

    def reset_button_settings(self):
        label, repeating, downAction, upAction = self.orig_settings
        self.repeating = repeating
        self.downAction = downAction
        self.upAction = upAction


    def run_down_action(self):
        if not self.repeating:
            if not self.pressed:
                self.downAction()
                self.pressed = True
                self.times_pressed += 1
        else:
            self.downAction()
            self.times_pressed += 1

    # Assuming the up function is non-repeating
    def run_up_action(self):
        if self.pressed:
            self.upAction()
            self.pressed = False



#BUTTONS = [
#		Button('A', False, pyautogui.mouseDown, pyautogui.mouseUp),
#		Button('B', False, lambda: pyautogui.click(button='right'), empty_function),
#		Button('C', True, lambda: pyautogui.scroll(-SCROLL_SPEED), empty_function),
#		Button('D', True, lambda: pyautogui.scroll(+SCROLL_SPEED), empty_function),
#		Button('E', False, lambda: M.set_mouse_speed("DECREASE"), lambda: M.set_mouse_speed("RESET")),
#		Button('F', False, lambda: M.set_mouse_speed("INCREASE"), lambda: M.set_mouse_speed("RESET")),
#		Button('JS_DOWN', False, lambda: M.toggle_mode(), empty_function)]
#
#BUTTON_MAP = {b.label: b for b in BUTTONS}

SCROLL_SPEED = 25
MOUSE_SPEED = 20

#M = Mouse(MOUSE_SPEED, lambda x: exponent(3,x))
#M = Mouse(MOUSE_SPEED, lambda x: simple_polynomial(x,3), BUTTON_MAP)
M = Mouse(MOUSE_SPEED, lambda x: simple_polynomial(x,3), {})

number_to_button = {
        0: "A",
        1: "B",
        2: "C",
        3: "D",
        4: "E",
        5: "F",
        6: "JS_DOWN" 
        }

# Move this into mouse eventually
def handleButtonState(state):
    for i in range(7):
        # Check which buttons are pressed
        if button_state >> i & 1:
            M.buttons[number_to_button[i]].run_down_action()
        else:
            M.buttons[number_to_button[i]].run_up_action()

first_time = True
while True:
    data = arduino.readline().decode('utf-8')

    if data:
        # Kill the first input, because it has some bytes from bootloader
        if first_time:			
            first_time = False
        else:
            js_x_pos, js_y_pos, button_state = [x.rstrip() for x in str(data).split(',') ]

            #try:
            #	js_x_pos = int(js_x_pos)
            #except ValueError:
            #	print("Oops!  That was no valid number.  Try again...")

            js_x_pos = int(js_x_pos)

            js_y_pos = int(js_y_pos)
            button_state = int(button_state)
            #print(js_x_pos, js_y_pos, button_state)

            M.run_mouse_movement(js_x_pos, js_y_pos * -1)
            M.perform_automatic_operations()
            M.ticks_alive += 1

            print(M.buttons['F'].times_pressed)
            handleButtonState(button_state)
